let btnw = document.getElementById('btn_work');
let list = document.querySelector('.work_list');
let btnC = document.getElementById('btn_company');
let clist = document.querySelector('.site');
// console.log(btnw, btnC, list, clist);

btnw.addEventListener('click', function(){
    btnw.classList.toggle('on');
    list.classList.toggle('on');
    btnC.classList.remove('on');
    clist.classList.remove('on');
});

btnC.addEventListener('click', function(){
    btnC.classList.toggle('on');
    clist.classList.toggle('on');
    btnw.classList.remove('on');
    list.classList.remove('on');
});

// ~~~~~~~~~~ 상단 검색창 기능 ~~~~~~~~~~
let sct = document.querySelector('.search_box');
// console.log(sct)
let scfo = document.querySelector('.search_form');

sct.addEventListener('click', function(){
    // console.log('click')
   scfo.classList.toggle('on');
});
// console.log(scfo);

let btnPause = document.querySelector('.btn_pause')
    console.log(btnPause);
let sw = true;
btnPause.addEventListener('click', function(){
    // switch 기법

    console.log("클릭")
    if(sw==true){
        btnPause.classList.add("on")
        swiper.autoplay.stop()
        sw=false
    }
    else{
        btnPause.classList.remove("on")
        swiper.autoplay.start()
    }
})

// content box 관련 기능
let conO = document.querySelector('.all');
let conBO = document.querySelector('.sitemap_wrap');
let conC = document.querySelector('.btn_close');
// console.log(conC, conO);

conO.addEventListener('click', function(){
    conBO.classList.toggle('on');
});
conC.addEventListener('click', function(){
    conBO.classList.remove('on');
});

// test